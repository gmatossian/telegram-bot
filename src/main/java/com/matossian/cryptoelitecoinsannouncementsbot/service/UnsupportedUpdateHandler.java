package com.matossian.cryptoelitecoinsannouncementsbot.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.matossian.cryptoelitecoinsannouncementsbot.dto.Update;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.response.Response;

@Component
public class UnsupportedUpdateHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(UnsupportedUpdateHandler.class);

	public Response handle(Update update) {
		LOGGER.warn("Ignoring update...");
		return new Response();
	}

}
