package com.matossian.cryptoelitecoinsannouncementsbot.dto.response;

import org.junit.Test;

import static org.junit.Assert.*;

public class ForwardMessageResponseTest {

	@Test
	public void testConstructor() {
		ForwardMessageResponse response = new ForwardMessageResponse("foo", "bar", "baz");
		assertEquals(response.chat_id, "foo");
		assertEquals(response.from_chat_id, "bar");
		assertEquals(response.message_id, "baz");
		assertEquals(response.method, "forwardMessage");
	}
}
