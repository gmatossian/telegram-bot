package com.matossian.cryptoelitecoinsannouncementsbot.dto;

public class MessageBuilder {

	private Message message;
	
	public MessageBuilder() {
		this.message = new Message();
	}
	
	public static MessageBuilder buildMessage() {
		return new MessageBuilder();
	}
	
	public MessageBuilder withId(int id) {
		this.message.message_id = id;
		return this;
	}
	
	public MessageBuilder fromUser(User user) {
		this.message.from = user;
		return this;
	}
	
	public MessageBuilder fromChat(Chat chat) {
		this.message.chat = chat;
		return this;
	}
	
	public Message build() {
		return this.message;
	}
}
