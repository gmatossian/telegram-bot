package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Class representing a User object.
 * See https://core.telegram.org/bots/api#user for details
 * 
 * @author gabo
 */
@ToString
@EqualsAndHashCode
public class User {
	
	public int id;
	public boolean is_bot;
	public String first_name;
	public String last_name;
	public String username;
	public String language_code;
	
}
