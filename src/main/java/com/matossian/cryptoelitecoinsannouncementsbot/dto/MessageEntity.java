package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Class representing a MessageEntity object
 * See https://core.telegram.org/bots/api#messageentity for details
 * 
 * @author gabo
 */
@ToString
@EqualsAndHashCode
public class MessageEntity {
	
	public String type;
	public int offset;
	public int length;
	public String url;
	public User user;

}
