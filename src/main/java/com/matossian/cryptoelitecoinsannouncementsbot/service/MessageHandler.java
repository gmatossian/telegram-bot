package com.matossian.cryptoelitecoinsannouncementsbot.service;

import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.matossian.cryptoelitecoinsannouncementsbot.dto.Message;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.response.ForwardMessageResponse;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.response.Response;

@Component
public class MessageHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageHandler.class);
	
	@Value("${admin.user.ids}")
	private int[] adminUserIds;
	
	@Value("${cryptoEliteAltcoins.group.id}")
	private String groupChatId;
	
	@Value("${cryptoEliteAltcoinsAnnouncements.group.id}")
	private String announcementsGroupChatId;

	public Response handle(Message message) {
		
		if (message.from != null
				&& message.chat != null
				&& IntStream.of(adminUserIds).anyMatch(x -> x == message.from.id)
				&& groupChatId.equals(message.chat.id)) {
			LOGGER.info("Forwarding message...");
			return new ForwardMessageResponse(announcementsGroupChatId,
					message.chat.id,
					String.valueOf(message.message_id));
		} else {
			LOGGER.info("Ignoring message...");
			return new Response();	
		}
	}

}
