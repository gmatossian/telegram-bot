package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Class representing a PhotoSize object
 * See https://core.telegram.org/bots/api#photosize for details
 * 
 * @author gabo
 */
@ToString
@EqualsAndHashCode
public class PhotoSize {

	public String file_id;
	public int width;
	public int height;
	public int file_size;
}
