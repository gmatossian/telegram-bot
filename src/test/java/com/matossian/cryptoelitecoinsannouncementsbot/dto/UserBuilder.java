package com.matossian.cryptoelitecoinsannouncementsbot.dto;

public class UserBuilder {

	private User user;
	
	public UserBuilder() {
		this.user = new User();
	}
	
	public static UserBuilder buildUser() {
		return new UserBuilder();
	}
	
	public UserBuilder withId(int id) {
		this.user.id = id;
		return this;
	}
	
	public User build() {
		return this.user;
	}
}
