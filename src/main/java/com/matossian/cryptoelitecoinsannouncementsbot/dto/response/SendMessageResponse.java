package com.matossian.cryptoelitecoinsannouncementsbot.dto.response;

import lombok.Data;

@Data
public class SendMessageResponse extends Response {

	public String method = "sendMessage";
	public String text;
	public String chat_id;
	
	public SendMessageResponse() {
	}
	
	public SendMessageResponse(String text, String chat_id) {
		this.text = text;
		this.chat_id = chat_id;
	}
}
