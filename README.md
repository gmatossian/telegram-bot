# README #

Telegram Bot that forwards messages from a group onto a different group.
The idea is to forward messages from users you're interested in onto a different group so you can just mute notifications on the original group and get notified instead only by the messages you're interested in.
See https://gmatossian.wordpress.com/2017/12/27/telegram-bot/ for details.