package com.matossian.cryptoelitecoinsannouncementsbot;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.ChatBuilder;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.MessageBuilder;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.Update;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.UpdateBuilder;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.UserBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BotApplicationTests {

    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void returnsForwardMessageResponseForAdminMessages() throws Exception {
    	Update update = UpdateBuilder
    			.buildUpdate()
    			.withMessage(MessageBuilder
    					.buildMessage()
    					.fromUser(UserBuilder
    							.buildUser()
    							.withId(429746285)
    							.build())
    					.fromChat(ChatBuilder
    							.buildChat()
    							.withId("-1001210575854")
    							.build())
    					.withId(4204)
    					.build())
    			.build();

        this.mockMvc.perform(
        		post("/bot")
        		.contentType(MediaType.APPLICATION_JSON)
        		.content(objectMapper.writeValueAsBytes(update)))
        	.andExpect(status().isOk())
        	.andExpect(jsonPath("$.*", hasSize(4)))
        	.andExpect(jsonPath("$.method", is("forwardMessage")))
        	.andExpect(jsonPath("$.chat_id", is("-253315974")))
        	.andExpect(jsonPath("$.from_chat_id", is("-1001210575854")))
        	.andExpect(jsonPath("$.message_id", is("4204")));
    }

    @Test
    public void returnsForwardMessageResponseForEditedAdminMessages() throws Exception {
    	Update update = UpdateBuilder
    			.buildUpdate()
    			.withEditedMessage(MessageBuilder
    					.buildMessage()
    					.fromUser(UserBuilder
    							.buildUser()
    							.withId(429746285)
    							.build())
    					.fromChat(ChatBuilder
    							.buildChat()
    							.withId("-1001210575854")
    							.build())
    					.withId(4204)
    					.build())
    			.build();

        this.mockMvc.perform(
        		post("/bot")
        		.contentType(MediaType.APPLICATION_JSON)
        		.content(objectMapper.writeValueAsBytes(update)))
        	.andExpect(status().isOk())
        	.andExpect(jsonPath("$.*", hasSize(4)))
        	.andExpect(jsonPath("$.method", is("forwardMessage")))
        	.andExpect(jsonPath("$.chat_id", is("-253315974")))
        	.andExpect(jsonPath("$.from_chat_id", is("-1001210575854")))
        	.andExpect(jsonPath("$.message_id", is("4204")));
    }

    @Test
    public void returnsEmptyResponseForNonAdminMessages() throws Exception {
    	Update update = UpdateBuilder
    			.buildUpdate()
    			.withMessage(MessageBuilder
    					.buildMessage()
    					.fromUser(UserBuilder
    							.buildUser()
    							.withId(111111111)
    							.build())
    					.fromChat(ChatBuilder
    							.buildChat()
    							.withId("-1001210575854")
    							.build())
    					.withId(4204)
    					.build())
    			.build();

        this.mockMvc.perform(
        		post("/bot")
        		.contentType(MediaType.APPLICATION_JSON)
        		.content(objectMapper.writeValueAsBytes(update)))
        	.andExpect(status().isOk())
        	.andExpect(jsonPath("$.*", hasSize(0)));
    }

    @Test
    public void returnsEmptyResponseForNonAdminEditedMessages() throws Exception {
    	Update update = UpdateBuilder
    			.buildUpdate()
    			.withEditedMessage(MessageBuilder
    					.buildMessage()
    					.fromUser(UserBuilder
    							.buildUser()
    							.withId(111111111)
    							.build())
    					.fromChat(ChatBuilder
    							.buildChat()
    							.withId("-1001210575854")
    							.build())
    					.withId(4204)
    					.build())
    			.build();

        this.mockMvc.perform(
        		post("/bot")
        		.contentType(MediaType.APPLICATION_JSON)
        		.content(objectMapper.writeValueAsBytes(update)))
        	.andExpect(status().isOk())
        	.andExpect(jsonPath("$.*", hasSize(0)));
    }

    @Test
    public void returnsEmptyResponseForMessagesFromDifferentGroup() throws Exception {
    	Update update = UpdateBuilder
    			.buildUpdate()
    			.withMessage(MessageBuilder
    					.buildMessage()
    					.fromUser(UserBuilder
    							.buildUser()
    							.withId(429746285)
    							.build())
    					.fromChat(ChatBuilder
    							.buildChat()
    							.withId("1111111111111")
    							.build())
    					.withId(4204)
    					.build())
    			.build();

        this.mockMvc.perform(
        		post("/bot")
        		.contentType(MediaType.APPLICATION_JSON)
        		.content(objectMapper.writeValueAsBytes(update)))
        	.andExpect(status().isOk())
        	.andExpect(jsonPath("$.*", hasSize(0)));
    }

    @Test
    public void returnsEmptyResponseForEditedMessagesFromDifferentGroup() throws Exception {
    	Update update = UpdateBuilder
    			.buildUpdate()
    			.withEditedMessage(MessageBuilder
    					.buildMessage()
    					.fromUser(UserBuilder
    							.buildUser()
    							.withId(429746285)
    							.build())
    					.fromChat(ChatBuilder
    							.buildChat()
    							.withId("1111111111111")
    							.build())
    					.withId(4204)
    					.build())
    			.build();

        this.mockMvc.perform(
        		post("/bot")
        		.contentType(MediaType.APPLICATION_JSON)
        		.content(objectMapper.writeValueAsBytes(update)))
        	.andExpect(status().isOk())
        	.andExpect(jsonPath("$.*", hasSize(0)));
    }

}
