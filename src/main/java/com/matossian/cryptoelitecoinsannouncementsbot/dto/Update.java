package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import lombok.Data;
import lombok.ToString;

/**
 * Class representing an Update object
 * See https://core.telegram.org/bots/api#update for details
 * 
 * NOTE: Some fields are NOT being mapped:
 * -  inline_query
 * - chosen_inline_result
 * - callback_query
 * - shipping_query
 * - pre_checkout_query
 * 
 * @author gabo
 */
@Data
@ToString
public class Update {

	public int update_id;
	public Message message;
	public Message edited_message;
	public Message channel_post;
	public Message edited_channel_post;
	
}
