package com.matossian.cryptoelitecoinsannouncementsbot.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.matossian.cryptoelitecoinsannouncementsbot.dto.ChatBuilder;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.Message;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.MessageBuilder;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.UserBuilder;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.response.ForwardMessageResponse;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.response.Response;

public class MessageHandlerTest {
	
	private MessageHandler handler;
	
	@Before
	public void setUp() {
		handler = new MessageHandler();
		ReflectionTestUtils.setField(handler, "adminUserIds", new int [] {1,2});
		ReflectionTestUtils.setField(handler, "groupChatId", "foo");
		ReflectionTestUtils.setField(handler, "announcementsGroupChatId", "bar");
	}

	@Test
	public void testHandleWithMessageFromAdmin() {
		Message message = MessageBuilder
		.buildMessage()
		.fromUser(UserBuilder
				.buildUser()
				.withId(1)
				.build())
		.fromChat(ChatBuilder
				.buildChat()
				.withId("foo")
				.build())
		.withId(999)
		.build();
		
		Response actual = handler.handle(message);
		
		assertThat(actual instanceof ForwardMessageResponse).isTrue();
		ForwardMessageResponse fwMsgResp = (ForwardMessageResponse) actual;
		assertThat(fwMsgResp.chat_id).isEqualTo("bar");
		assertThat(fwMsgResp.from_chat_id).isEqualTo("foo");
		assertThat(fwMsgResp.message_id).isEqualTo("999");
	}

	@Test
	public void testHandleWithMessageNotFromAdmin() {
		Message message = MessageBuilder
		.buildMessage()
		.fromUser(UserBuilder
				.buildUser()
				.withId(3)
				.build())
		.fromChat(ChatBuilder
				.buildChat()
				.withId("foo")
				.build())
		.withId(999)
		.build();
		
		Response actual = handler.handle(message);
		
		assertThat(actual instanceof ForwardMessageResponse).isFalse();
		assertThat(actual).isEqualTo(new Response());
	}

	@Test
	public void testHandleWithMessageFromOtherGroup() {
		Message message = MessageBuilder
		.buildMessage()
		.fromUser(UserBuilder
				.buildUser()
				.withId(1)
				.build())
		.fromChat(ChatBuilder
				.buildChat()
				.withId("differentGroup")
				.build())
		.withId(999)
		.build();
		
		Response actual = handler.handle(message);
		
		assertThat(actual instanceof ForwardMessageResponse).isFalse();
		assertThat(actual).isEqualTo(new Response());
	}
}
