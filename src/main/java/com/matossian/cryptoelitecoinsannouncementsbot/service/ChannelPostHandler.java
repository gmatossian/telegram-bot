package com.matossian.cryptoelitecoinsannouncementsbot.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.matossian.cryptoelitecoinsannouncementsbot.dto.Message;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.response.Response;

@Component
public class ChannelPostHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChannelPostHandler.class);

	public Response handle(Message message) {
		LOGGER.warn("Ignoring channel post...");
		return new Response();
	}

}
