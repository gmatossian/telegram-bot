package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class UserBuilderTest {

	@Test
	public void testBuilderWithNoValues() {
		User actual = UserBuilder
				.buildUser()
				.build();
		
		User expected = new User();
		
		assertThat(actual).isEqualTo(expected);
	}

	@Test
	public void testBuilderSettingId() {
		User actual = UserBuilder
				.buildUser()
				.withId(123)
				.build();
		
		User expected = new User();
		expected.id = 123;
		
		assertThat(actual).isEqualTo(expected);
	}
}
