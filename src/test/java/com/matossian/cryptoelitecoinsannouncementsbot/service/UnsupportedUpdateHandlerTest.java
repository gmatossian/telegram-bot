package com.matossian.cryptoelitecoinsannouncementsbot.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import com.matossian.cryptoelitecoinsannouncementsbot.dto.Update;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.response.Response;

public class UnsupportedUpdateHandlerTest {

	@Test
	public void testHandle() {
		UnsupportedUpdateHandler handler = new UnsupportedUpdateHandler();
		
		Update update = new Update();
		
		Response actual = handler.handle(update);
		
		Response expected = new Response();
		
		assertThat(actual).isEqualTo(expected);
	}
}
