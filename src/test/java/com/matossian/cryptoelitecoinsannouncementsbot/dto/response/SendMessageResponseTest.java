package com.matossian.cryptoelitecoinsannouncementsbot.dto.response;

import org.junit.Test;

import static org.junit.Assert.*;

public class SendMessageResponseTest {

	@Test
	public void testConstructor() {
		SendMessageResponse response = new SendMessageResponse("foo", "bar");
		assertEquals(response.text, "foo");
		assertEquals(response.chat_id, "bar");
		assertEquals(response.method, "sendMessage");
	}
}
