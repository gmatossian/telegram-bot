package com.matossian.cryptoelitecoinsannouncementsbot.dto.response;

import lombok.Data;

@Data
public class ForwardMessageResponse extends Response {

	public String method = "forwardMessage";
	public String chat_id;
	public String from_chat_id;
	public String message_id;

	public ForwardMessageResponse() {
	}
	
	public ForwardMessageResponse(String chat_id, String from_chat_id, String message_id) {
		this.chat_id = chat_id;
		this.from_chat_id = from_chat_id;
		this.message_id = message_id;
	}

}
