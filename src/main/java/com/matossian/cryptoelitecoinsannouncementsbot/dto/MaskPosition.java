package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Class representing a MaskPosition object
 * See https://core.telegram.org/bots/api#maskposition for details
 * 
 * @author gabo
 */
@ToString
@EqualsAndHashCode
public class MaskPosition {

	public String point;
	public double x_shift;
	public double y_shift;
	public double scale;
}
