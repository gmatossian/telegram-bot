package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Class representing a Chat object
 * See https://core.telegram.org/bots/api#chat for details
 * 
 * NOTE: Some fields are NOT being mapped:
 * - photo
 * 
 * @author gabo
 */
@ToString
@EqualsAndHashCode
public class Chat {

	public String id;
	public String type;
	public String title;
	public String username;
	public String first_name;
	public String last_name;
	public boolean all_members_are_administrators;
	public String description;
	public String invite_link;
	public Message pinned_message;
	public String sticker_set_name;
	public boolean can_set_sticker_set;
	
}
