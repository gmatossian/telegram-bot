package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Class representing a Sticker object
 * See https://core.telegram.org/bots/api#sticker for details
 * 
 * @author gabo
 */
@ToString
@EqualsAndHashCode
public class Sticker {

	public String file_id;
	public int width;
	public int height;
	public PhotoSize thumb;
	public String emoji;
	public String set_name;
	public MaskPosition mask_position;
	public int file_size;
}
