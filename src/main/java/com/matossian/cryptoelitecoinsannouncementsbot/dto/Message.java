package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Class representing a Message object
 * See https://core.telegram.org/bots/api#message for details
 * 
 * NOTE: Some fields are NOT being mapped:
 * -  audio
 * - game
 * - video
 * - voice
 * - voice_note
 * - contact
 * - location
 * - venue
 * - invoice
 * - successful_payment
 * 
 * @author gabo
 */
@ToString
@EqualsAndHashCode
public class Message {

	public int message_id;
	public User from;
	public int date;
	public Chat chat;
	public User forward_from;
	public Chat forward_from_chat;
	public int forward_from_message_id;
	public String forward_signature;
	public int forward_date;
	public Message reply_to_message;
	public int edit_date;
	public String media_group_id;
	public String author_signature;
	public String text;
	public MessageEntity[] entities;
	public MessageEntity[] caption_entities;
	public Document document;
	public PhotoSize[] photo;
	public Sticker sticker;
	public String caption;
	public User[] new_chat_members;
	public User left_chat_member;
	public String new_chat_title;
	public PhotoSize[] new_chat_photo;
	public boolean delete_chat_photo;
	public boolean group_chat_created;
	public boolean supergroup_chat_created;
	public boolean channel_chat_created;
	public int migrate_to_chat_id;
	public int migrate_from_chat_id;
	public Message pinned_message;
	
}
