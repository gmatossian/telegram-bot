package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class ChatBuilderTest {

	@Test
	public void testBuilderWithNoValues() {
		Chat actual = ChatBuilder
				.buildChat()
				.build();
		
		Chat expected = new Chat();
		
		assertThat(actual).isEqualTo(expected);
	}

	@Test
	public void testBuilderSettingId() {
		Chat actual = ChatBuilder
				.buildChat()
				.withId("foo")
				.build();
		
		Chat expected = new Chat();
		expected.id = "foo";
		
		assertThat(actual).isEqualTo(expected);
	}
}
