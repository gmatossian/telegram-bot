package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class MessageBuilderTest {

	@Test
	public void testBuilderWithNoValues() {
		Message actual = MessageBuilder
				.buildMessage()
				.build();
		
		Message expected = new Message();
		
		assertThat(actual).isEqualTo(expected);
	}

	@Test
	public void testBuilderSettingId() {
		Message actual = MessageBuilder
				.buildMessage()
				.withId(123)
				.build();
		
		Message expected = new Message();
		expected.message_id = 123;
		
		assertThat(actual).isEqualTo(expected);
	}

	@Test
	public void testBuilderSettingUser() {
		User user = new User();
		user.id = 123;
		user.is_bot = true;
		user.first_name = "foo";
		
		Message actual = MessageBuilder
				.buildMessage()
				.fromUser(user)
				.build();
		
		Message expected = new Message();
		expected.from = user;
		
		assertThat(actual).isEqualTo(expected);
	}

	@Test
	public void testBuilderSettingChat() {
		Chat chat = new Chat();
		chat.id = "foo";
		chat.type = "bar";
		chat.title = "baz";
		
		Message actual = MessageBuilder
				.buildMessage()
				.fromChat(chat)
				.build();
		
		Message expected = new Message();
		expected.chat = chat;
		
		assertThat(actual).isEqualTo(expected);
	}
}
