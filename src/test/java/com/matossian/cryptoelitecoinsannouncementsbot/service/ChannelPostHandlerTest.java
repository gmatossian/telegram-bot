package com.matossian.cryptoelitecoinsannouncementsbot.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import com.matossian.cryptoelitecoinsannouncementsbot.dto.Message;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.response.Response;

public class ChannelPostHandlerTest {

	@Test
	public void testHandle() {
		ChannelPostHandler handler = new ChannelPostHandler();
		
		Message message = new Message();
		
		Response actual = handler.handle(message);
		
		Response expected = new Response();
		
		assertThat(actual).isEqualTo(expected);
	}
}
