package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class UpdateBuilderTest {

	@Test
	public void testBuilderWithNoValues() {
		Update actual = UpdateBuilder
				.buildUpdate()
				.build();
		
		Update expected = new Update();
		
		assertThat(actual).isEqualTo(expected);
	}

	@Test
	public void testBuilderSettingMessage() {
		Message message = new Message();
		message.message_id = 123;
		
		Update actual = UpdateBuilder
				.buildUpdate()
				.withMessage(message)
				.build();
		
		Update expected = new Update();
		expected.message = new Message();
		expected.message.message_id = 123;
		
		assertThat(actual).isEqualTo(expected);
	}

	@Test
	public void testBuilderSettingEditedMessage() {
		Message message = new Message();
		message.message_id = 123;
		
		Update actual = UpdateBuilder
				.buildUpdate()
				.withEditedMessage(message)
				.build();
		
		Update expected = new Update();
		expected.edited_message = new Message();
		expected.edited_message.message_id = 123;
		
		assertThat(actual).isEqualTo(expected);
	}

}
