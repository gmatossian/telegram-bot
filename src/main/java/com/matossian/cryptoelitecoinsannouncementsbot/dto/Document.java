package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Class representing a Document object
 * See https://core.telegram.org/bots/api#document for details
 * 
 * @author gabo
 */
@ToString
@EqualsAndHashCode
public class Document {

	public String file_id;
	public PhotoSize thumb;
	public String file_name;
	public String mime_type;
	public Integer file_size;
}
