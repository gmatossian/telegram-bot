package com.matossian.cryptoelitecoinsannouncementsbot.dto.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;

/**
 * Base class representing a response 
 * 
 * @author gabo
 */
@Data
@JsonSerialize
public class Response {
}
