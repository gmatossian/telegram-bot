package com.matossian.cryptoelitecoinsannouncementsbot.web;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.Update;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.response.Response;
import com.matossian.cryptoelitecoinsannouncementsbot.service.ChannelPostHandler;
import com.matossian.cryptoelitecoinsannouncementsbot.service.MessageHandler;
import com.matossian.cryptoelitecoinsannouncementsbot.service.UnsupportedUpdateHandler;

@RestController
public class BotController {

	private static final Logger LOGGER = LoggerFactory.getLogger(BotController.class);
	
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	
	@Autowired
	private MessageHandler messageHandler;
	
	@Autowired
	private ChannelPostHandler channelPostHandler;
	
	@Autowired
	private UnsupportedUpdateHandler unsupportedUpdateHandler;

	@RequestMapping("/bot")
    public Response handleUpdateRequest(@Valid @RequestBody Update update) {
		try {
			LOGGER.info("Update received: " + OBJECT_MAPPER.writeValueAsString(update));
		} catch (JsonProcessingException e) {
			LOGGER.debug("Failed to serialize update as a String", e);
		}
		
		if (update.message != null) {
			LOGGER.info("Handling new message...");
			return messageHandler.handle(update.message);
		} else if (update.edited_message != null) {
			LOGGER.info("Handling edited message...");
			return messageHandler.handle(update.edited_message);
		} else if (update.channel_post != null) {
			LOGGER.info("Handling new channel post...");
			return channelPostHandler.handle(update.channel_post);
		} else if (update.edited_channel_post != null) {
			LOGGER.info("Handling edited channel post...");
			return channelPostHandler.handle(update.edited_channel_post);
		} else {
			LOGGER.info("Handling unsupported update type...");
			return unsupportedUpdateHandler.handle(update);
		}
    }
}
