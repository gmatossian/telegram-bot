package com.matossian.cryptoelitecoinsannouncementsbot.dto;

public class ChatBuilder {
	
	private Chat chat;
	
	public ChatBuilder () {
		this.chat = new Chat();
	}
	
	public static ChatBuilder buildChat() {
		return new ChatBuilder();
	}
	
	public ChatBuilder withId(String id) {
		this.chat.id = id;
		return this;
	}
	
	public Chat build() {
		return this.chat;
	}

}
