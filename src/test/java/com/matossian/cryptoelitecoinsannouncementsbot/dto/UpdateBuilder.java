package com.matossian.cryptoelitecoinsannouncementsbot.dto;

import com.matossian.cryptoelitecoinsannouncementsbot.dto.Message;
import com.matossian.cryptoelitecoinsannouncementsbot.dto.Update;

public class UpdateBuilder {

	private Update update;
	
	public UpdateBuilder() {
		this.update = new Update();
	}
	
	public static UpdateBuilder buildUpdate() {
		return new UpdateBuilder();
	}
	
	public UpdateBuilder withMessage(Message message) {
		this.update.message = message;
		return this;
	}
	
	public UpdateBuilder withEditedMessage(Message message) {
		this.update.edited_message = message;
		return this;
	}
	
	public Update build() {
		return this.update;
	}
}
